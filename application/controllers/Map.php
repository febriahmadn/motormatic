<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {
    //function __construct() {
		//parent::__construct();
		//$level = $this->session->userdata('level');
		//if ($level!='admin') {
		//	redirect('404');
		//}

    public function index(){

        $this->load->library('googlemaps');

        //get lat long user
        $lat = (isset($_GET['lat']))?$_GET['lat'] : '';
        $lng = (isset($_GET['lng']))?$_GET['lng'] : '';

        $config['center'] = $lat.','.$lng;
        $config['zoom'] = 'auto';
        $this->googlemaps->initialize($config);

        $result = $this->curl->simple_get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$lat.','.$lng.'&keyword=bengkel+motor&radius=200&key=AIzaSyDrbY47RMsrjuzud_eZxtoobPxRmyTiBo8');
        $result = json_decode($result);
        $results = $result->results;

        //lokasiku
        $marker = array();
        $marker['position'] = $lat.','.$lng;
        $marker['onclick'] = 'alert("Posisi User")';
        $marker['onbclick'] = 
        $this->googlemaps->add_marker($marker);
        
        //lokasi bengkel
        $icon = ['http ke 1','http ke 2'];
        foreach ($results as $key => $value) {
          $marker = array();
          $url = 'https://www.google.com/maps/dir/'.$lat.','.$lng.'/'.$value->geometry->location->lat.','.$value->geometry->location->lng;
          
          $marker['position'] = $value->geometry->location->lat.','.$value->geometry->location->lng;
          $marker['onclick'] = 'window.open(\''.$url.'\',\'_blank\')';
            
          $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';

          //ini misal nerapin dari array
          // $marker['icon'] = $icon[$key];

          $marker['onbclick'] = 
          $this->googlemaps->add_marker($marker);
        }

        // $marker = array();
        // $marker['position'] = $lat.','.$lng;
        // $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
        // $this->googlemaps->add_marker($marker);

        // $marker = array();
        // $marker['position'] ='-7.298731, 112.7666358';
        // $marker['draggable'] = TRUE;
        // $marker['animation'] = 'DROP';
        // $this->googlemaps->add_marker($marker);

        // $marker = array();
        // $marker['position'] = '-7.298343, 112.7660618';
        // $marker['onclick'] = 'alert("Jangan sentuh aku !")';
        // $marker['onbclick'] = 
        // $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();
        $data['judul_web'] = "Peta Terdekat";

        $this->load->view('web/header', $data);
        $this->load->view('maps/index', $data);
        $this->load->view('web/footer');


        }

    //public function error_not_found()
      //  {
        //    $this->load->view('404_content');
        //}
}