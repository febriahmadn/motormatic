<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends CI_Controller {
	public function index(){
		$data['judul_web'] = $this->M_Web->judul_web();
		$data['status'] = 'Form Biodata Pengguna';
		$data['judul'] = $this->M_Web->judul_web();

		$this->load->view('web/header', $data);
		$this->load->view("web/diagnosa/form", $data);
		$this->load->view('web/footer');
		$this->db->order_by('kode_gejala','ASC');
		$this->db->limit(1);

		$kode_gejala = $this->db->get('tbl_gejala')->row()->kode_gejala;

		if ($this->session->userdata('level') == 'user') {
			$id_userx = $this->session->userdata('id_user');
			// redirect("diagnosa/p/$id_userx/$kode_gejala");
			redirect("diagnosa/gejalaawal");
		}

		date_default_timezone_set('Asia/Jakarta');
		$tgl = date('Y-m-d H:i:s');

		if (isset($_POST['btnmulai'])) {
			$nama   = htmlentities(strip_tags($this->input->post('nama')));
			$email  = htmlentities(strip_tags($this->input->post('email')));
			$no_hp  = htmlentities(strip_tags($this->input->post('no_hp')));
			$alamat = htmlentities(strip_tags($this->input->post('alamat')));
			$username = htmlentities(strip_tags($this->input->post('username')));
			// $id_user = md5("'$nama' $tgl $no_hp");
			$cek_un = $this->db->get_where('tbl_pakar', array('username'=>$username));
			$simpan = 'y';
			$pesan  = 'n';
			if ($cek_un->num_rows()!=0) {
				$simpan = 'n';
				$pesan  = "Username '<b>$username</b>' sudah ada";
			}else {
				$cek_un2 = $this->db->get_where('tbl_user', array('username'=>$username));
				if ($cek_un2->num_rows()!=0) {
					$simpan = 'n';
					$pesan  = "Username '<b>$username</b>' sudah ada";
				}
			}

			if ($simpan=='y') {
				$data = array(
					'id_user' => $id_user,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
					'alamat' => $alamat,
					'username' => $username,
					'password' => $username,
					'tgl_daftar' => $tgl
				);
				$this->db->insert('tbl_user',$data);

				$id_user = $this->db->insert_id();

				$this->session->set_userdata('username', "$username");
				$this->session->set_userdata('id_user', "$id_user");
				$this->session->set_userdata('nama', "$nama");
				$this->session->set_userdata('level', "user");

				redirect("diagnosa/gejalaawal");
			}else {
				$this->session->set_flashdata('msg',
					'
					<div class="alert alert-warning alert-dismissible" role="alert">
						 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							 <span aria-hidden="true">&times;&nbsp; &nbsp;</span>
						 </button>
						 <strong>Gagal!</strong> '.$pesan.'.
					</div>'
				);
				redirect('diagnosa');
			}
		}
	}

	public function simpangejala(){
		// print_r($_POST);
		// print_r($this->session);
		if(isset($_POST['gejala_penentu']) and $this->session){
			$kode_gejala = $_POST['gejala_penentu'];
			$user_id = $this->session->id_user;
			$data_diagnosa = $this->db->get_where('tbl_diagnosa', array('id_user'=>$user_id));
			$ke = $data_diagnosa->num_rows()+1;
			print_r($ke);
			date_default_timezone_set('Asia/Jakarta');
			$tgl = date('Y-m-d H:i:s');
			$data = array(
				'id_user' => $user_id,
				'kode_gejala' => $kode_gejala,
				'jawab' => 'Ya',
				'diagnosa_ke' => $ke,
				'tgl_diagnosa' => $tgl
			);
			$this->db->insert('tbl_diagnosa', $data);

			$data_kemungkinan = $this->db->get_where('tbl_kemungkinan', array('kode_gejala' => $kode_gejala, 'kode_gejala_sebelumnya' => $kode_gejala))->row();
			if($data_kemungkinan->kode_gejala_selanjutnya){
				redirect("diagnosa/p/$ke/$data_kemungkinan->kode_gejala_selanjutnya/$kode_gejala/");
			}
		}
	}

	public function gejalaawal(){
		$data['judul_web'] = $this->M_Web->judul_web();
		$data['status'] = 'Diagnosa';
		$data['judul'] = 'Jawablah pertanyaan berikut sesuai dengan gejala yang Anda rasakan';
		$data['list_gejala_penentu'] = $this->db->get_where('tbl_gejala', array('penentu'=>"yes"))->result_array();
		$this->load->view('web/header', $data);
		$this->load->view("web/diagnosa/gejalaawal", $data);
		$this->load->view('web/footer');
	}

	public function p($ke='', $kode_gejala_sebelumnya='', $kode_gejala=''){
		$id_user = $this->session->id_user;
		if ($id_user == '' or $kode_gejala == '') {
			redirect('404');
		}
		$data['judul_web'] = $this->M_Web->judul_web();
		$data['status'] = 'Diagnosa';
		$data['judul'] = 'Jawablah pertanyaan berikut sesuai dengan gejala yang Anda rasakan';
		$data['query'] = $this->db->get_where('tbl_gejala', "kode_gejala='$kode_gejala_sebelumnya'")->row();
		$data_kemungkinan = $this->db->get_where('tbl_kemungkinan', array('kode_gejala' => $kode_gejala, 'kode_gejala_sebelumnya' => $kode_gejala_sebelumnya))->row();
		// print_r($data_kemungkinan);
		$this->load->view('web/header', $data);
		$this->load->view("web/diagnosa/step", $data);
		$this->load->view('web/footer');

		if($_POST){
			$jawab = htmlentities(strip_tags($this->input->post('status')));
			print_r($jawab);
			date_default_timezone_set('Asia/Jakarta');
			$tgl = date('Y-m-d H:i:s');
			$data = array(
				'id_user' => $id_user,
				'kode_gejala' => $kode_gejala_sebelumnya,
				'jawab' => $jawab,
				'diagnosa_ke' => $ke,
				'tgl_diagnosa' => $tgl
			);
			$this->db->insert('tbl_diagnosa', $data);
			if($data_kemungkinan->kode_gejala_selanjutnya != ''){
				$kode_next = $data_kemungkinan->kode_gejala_selanjutnya;
				if($jawab == "ya"){
					redirect("diagnosa/p/$ke/$kode_next/$kode_gejala/");
				}else{
					$data_kemungkinan = $this->db->get_where('tbl_kemungkinan', array('kode_gejala' => $kode_gejala, 'kode_gejala_sebelumnya' => $kode_gejala_sebelumnya, 'bercabang' => 'Ya'))->row();
					if($data_kemungkinan->kode_gejala_selanjutnya != ''){
						$kode_next = $data_kemungkinan->kode_gejala_selanjutnya;
						redirect("diagnosa/p/$ke/$kode_next/$kode_gejala/");
					}
				}
			}else if($jawab == 'tidak'){
				$data_kemungkinan = $this->db->get_where('tbl_kemungkinan', array('kode_gejala' => $kode_gejala, 'kode_gejala_sebelumnya' => $kode_gejala_sebelumnya, 'bercabang' => 'Ya'))->row();
				if($data_kemungkinan->kode_gejala_selanjutnya != ''){
					$kode_next = $data_kemungkinan->kode_gejala_selanjutnya;
					redirect("diagnosa/p/$ke/$kode_next/$kode_gejala/");
				}
			}else{
				print_r($data_kemungkinan->kode_penyakit);
				if($data_kemungkinan->kode_penyakit != ''){
					$data = array(
						'id_user' => $id_user,
						'kode_gejala_penentu' => $kode_gejala,
						'diagnosa_ke' => $ke,
						'hasil' => $data_kemungkinan->kode_penyakit
					);
					$this->db->insert('tbl_hasil_diagnosa', $data);
					$insert_id = $this->db->insert_id();
					print_r($insert_id);
					redirect("diagnosa/hasildiagnosa/$insert_id");
				}else{
					redirect('diagnosa');
				}
			}
		}

		// $data['query'] = $this->db->get_where('tbl_gejala',"kode_gejala='$kode_gejala'")->row();
		// if ($data['query']->kode_gejala=='') {
		// 	redirect('404');
		// }
		// $data_diagnosa = $this->db->get_where('tbl_diagnosa', array('id_user'=>$id_user,'kode_gejala'=>$kode_gejala));
		// $ke = $data_diagnosa->num_rows()+1;
		// if ($data_diagnosa->num_rows()!=0) {
		// 	redirect('404');
		// }
		
		// date_default_timezone_set('Asia/Jakarta');
		// $tgl = date('Y-m-d H:i:s');
		// if (isset($_POST['btnstep'])) {
		// 	$cek_diagnosa = $this->db->get_where('tbl_diagnosa', array('id_user' => $id_user,'kode_gejala'=>$kode_gejala,'diagnosa_ke' => $ke));
		// 	if ($cek_diagnosa->num_rows() == 0) {
		// 		$jawab = htmlentities(strip_tags($this->input->post('status')));
		// 		$data = array(
		// 			'id_user' => $id_user,
		// 			'kode_gejala' => $kode_gejala,
		// 			'jawab' => $jawab,
		// 			'diagnosa_ke' => $ke,
		// 			'tgl_diagnosa' => $tgl
		// 		);
		// 		$this->db->insert('tbl_diagnosa', $data);
		// 	}

		// 	$cek_kode = $this->db->get_where('tbl_kemungkinan', array('kode_gejala' => "$kode_gejala"));
		// }
	}

	public function hasildiagnosa($hasildiagnosa_id='', $action=''){
		if($hasildiagnosa_id == ''){
			redirect('404');
		}
		if($this->session){
			$user_id = $this->session->id_user;
			$data_diagnosa = $this->db->get_where('tbl_hasil_diagnosa', "id_hasil_diagnosa='$hasildiagnosa_id'")->row();
			if($data_diagnosa){
				// print_r($data_diagnosa);
				$data['id_hasil_diagnosa'] = $hasildiagnosa_id;
				$data['judul_web'] = $this->M_Web->judul_web();
				$data['status'] = 'Hasil Diagnosa';
				$data['judul'] = "HASIL ".$this->M_Web->judul_web(2)." ".$this->M_Web->judul_web(3);

				$this->db->join('tbl_diagnosa','tbl_diagnosa.id_user=tbl_user.id_user');
				$data['query'] = $this->db->get_where('tbl_user', array('tbl_user.id_user' => $data_diagnosa->id_user,'diagnosa_ke'=>$data_diagnosa->diagnosa_ke))->row();

				if ($data['query']->id_user=='') {
					redirect('404');
				}

				$this->db->join('tbl_gejala',"tbl_gejala.kode_gejala=tbl_diagnosa.kode_gejala");
				$this->db->order_by('tbl_gejala.kode_gejala','ASC');
				$data['v_diagnosa'] = $this->db->get_where('tbl_diagnosa', array('id_user' => $data_diagnosa->id_user,'diagnosa_ke' => $data_diagnosa->diagnosa_ke));
				$data['diagnosa_ke'] = $data_diagnosa->diagnosa_ke;

				if($action == 'cetak'){
					$this->load->view("web/diagnosa/cetak", $data);
				}else{
					$this->load->view('web/header', $data);
					$this->load->view("web/diagnosa/hasil", $data);
					$this->load->view('web/footer');
				}
			}else{
				redirect('404');
			}
		}else{
			redirect('web/login');
		}
		
	}

	public function hasil($id_user='', $aksi='', $ke=''){
		if ($id_user=='') {
			redirect('404');
		}
		$data['judul_web'] = $this->M_Web->judul_web();
		$data['status'] = 'Hasil Diagnosa';
		$data['judul'] = "HASIL ".$this->M_Web->judul_web(2)." ".$this->M_Web->judul_web(3);
		$this->db->join('tbl_diagnosa','tbl_diagnosa.id_user=tbl_user.id_user');
		$data['query'] = $this->db->get_where('tbl_user', array('tbl_user.id_user' => $id_user,'diagnosa_ke'=>$ke))->row();
		if ($data['query']->id_user=='') {
			redirect('404');
		}

		$this->db->join('tbl_gejala',"tbl_gejala.kode_gejala=tbl_diagnosa.kode_gejala");
		$this->db->order_by('tbl_gejala.kode_gejala','ASC');
		$data['v_diagnosa'] = $this->db->get_where('tbl_diagnosa', array('id_user'=>$id_user,'diagnosa_ke'=>$ke));
		$data['diagnosa_ke'] = $ke;
		if ($aksi=='0') {
			$this->load->view('web/header', $data);
			$this->load->view("web/diagnosa/hasil", $data);
			$this->load->view('web/footer');
		}elseif ($aksi=='cetak') {
			$this->load->view("web/diagnosa/cetak", $data);
		}else {
			redirect('404');
		}
	}


	public function riwayat(){
		$ceks = $this->session->userdata('username');
		$id_user = $this->session->userdata('id_user');
		$level = $this->session->userdata('level');
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user'] = $this->db->get_where('tbl_pakar',"username='$ceks'");
			$data['judul_web'] = "Riwayat Diagnosa";
			$this->db->join('tbl_diagnosa','tbl_diagnosa.id_user=tbl_user.id_user');
			$this->db->group_by('tbl_user.id_user, diagnosa_ke');
			if ($level == 'user') {
				$this->db->where('tbl_user.id_user',$id_user);
			}
			$this->db->order_by('tgl_daftar','DESC');
			$this->db->order_by('diagnosa_ke','DESC');
			$data['sql'] = $this->db->get('tbl_user');
			$this->load->view('users/header', $data);
			$this->load->view('users/riwayat', $data);
			$this->load->view('users/footer');
		}
	}

	public function hapus($id='', $ke=''){
		$ceks = $this->session->userdata('username');
		$id_user = $this->session->userdata('id_user');
		$level = $this->session->userdata('level');
		if ($ke=='') {
			redirect('404');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']   	 = $this->db->get_where('tbl_pakar',"username='$ceks'");
			if ($level=='user') {
				$id = $id_user;
			}
	  		$this->db->where('id_user',"$id");
			$cek_user = $this->db->get('tbl_user');
			if ($cek_user->num_rows()==0) {
				$this->session->set_flashdata('msg',
					'
					<div class="alert alert-warning alert-dismissible" role="alert">
						 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							 <span aria-hidden="true">&times;&nbsp; &nbsp;</span>
						 </button>
						 <strong>Gagal!</strong> ID tidak ditemukan, Silahkan coba lagi.
					</div>'
				);
			}else {
				$this->db->delete('tbl_diagnosa', array('id_user' => $id,'diagnosa_ke'=>$ke));
				if ($this->db->get_where('tbl_diagnosa', array('id_user'=>$id_user))->num_rows()==0) {
						$this->db->delete('tbl_user', array('id_user' => $id));
				}
				$this->session->set_flashdata('msg',
					'
					<div class="alert alert-success alert-dismissible" role="alert">
						 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							 <span aria-hidden="true">&times;&nbsp; &nbsp;</span>
						 </button>
						 <strong>Sukses!</strong> Berhasil dihapus.
					</div>'
				);
			}
			redirect('diagnosa/riwayat');
		}
	}
}